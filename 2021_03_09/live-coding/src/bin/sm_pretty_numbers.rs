use std::{
    sync::{Arc, Condvar, Mutex},
    thread,
    time::Duration,
};

use rand::{thread_rng, Rng};

fn main() {
    let mutex = Mutex::new(PrettyNumber::None);
    let condition = Condvar::new();
    let pair = Arc::new((mutex, condition));
    let alive = Arc::downgrade(&pair);
    let lock_primes = pair.clone();
    let lock_squares = pair.clone();
    let lock_powers = pair;
    let _handle_primes = thread::spawn(|| find_prime(lock_primes));
    let _handle_squares = thread::spawn(|| find_square(lock_squares));
    let _handle_powers = thread::spawn(|| find_power_of_two(lock_powers));

    loop {
        if let Some(pair) = alive.upgrade() {
            let (mutex, cvar) = &*pair;
            let (mut number, _) = cvar
                .wait_timeout_while(mutex.lock().unwrap(), Duration::from_micros(10), |number| {
                    *number == PrettyNumber::None
                })
                .unwrap();
            if *number != PrettyNumber::None {
                println!("Isn't it pretty?? {:?}", number);
                *number = PrettyNumber::None;
                cvar.notify_all();
            }
            match alive.upgrade() {
                Some(_) => {}
                None => return,
            }
            thread::sleep(Duration::from_micros(10));
        } else {
            return;
        }
    }
}

fn find_prime(lock: Arc<(Mutex<PrettyNumber>, Condvar)>) {
    for _ in 0..10000 {
        let candidate = thread_rng().gen::<u16>();
        if check_prime(candidate) {
            register_number(1, &lock, PrettyNumber::Prime(candidate));
        }
    }
    println!("Terminating Primes");
}

fn find_square(lock: Arc<(Mutex<PrettyNumber>, Condvar)>) {
    for _ in 0..10000 {
        let candidate = thread_rng().gen::<u16>();
        if (candidate as f64).sqrt().fract() == 0.0 {
            register_number(2, &lock, PrettyNumber::Square(candidate));
        }
    }
    println!("Terminating Squares");
}

fn find_power_of_two(lock: Arc<(Mutex<PrettyNumber>, Condvar)>) {
    for _ in 0..10000 {
        let candidate = thread_rng().gen::<u16>();
        if candidate.is_power_of_two() {
            register_number(3, &lock, PrettyNumber::PowerOfTwo(candidate));
        }
    }
    println!("Terminating Powers");
}

fn register_number(id: usize, lock: &Arc<(Mutex<PrettyNumber>, Condvar)>, pn: PrettyNumber) {
    let (mutex, cvar) = &**lock;
    println!("{} enters waiting room.", id);
    let mut number = cvar
        .wait_while(mutex.lock().unwrap(), |number| {
            *number != PrettyNumber::None
        })
        .unwrap();
    *number = pn;
    drop(number);
    println!("{} leaves waiting room.", id);
    cvar.notify_all();
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum PrettyNumber {
    Prime(u16),
    PowerOfTwo(u16),
    Square(u16),
    None,
}

fn check_prime(count: u16) -> bool {
    //! Doesn't really work, but that suffices for the intention.
    if count.rem_euclid(2) == 0 {
        return false;
    }
    let stop = ((count as f64).sqrt() + 1.0) as u16;
    for i in 3..stop {
        if i.rem_euclid(2) != 0 {
            if count.rem_euclid(i) == 0 {
                return false;
            }
        }
    }
    return true;
}
