#![no_std]
#![no_main]
#![feature(type_alias_impl_trait)]

use defmt_rtt as _; // global logger
use nrf52840_hal::pac;
use panic_probe as _;

use defmt::*;

defmt::timestamp!("{=u32:us}", (pac::DWT::cycle_count() / 1000) * 16);

use embassy::executor::Spawner;
use embassy::time::{Duration, Timer};
use embassy_nrf::{
    gpio::{Level, Output, OutputDrive},
    interrupt,
    peripherals::P0_13,
    peripherals::P0_14,
    temp::Temp,
    Peripherals,
};

#[embassy::task]
async fn blinker1(mut led: Output<'static, P0_13>, interval: Duration) {
    loop {
        led.set_high();
        Timer::after(interval).await;
        led.set_low();
        Timer::after(interval).await;
    }
}

#[embassy::task]
async fn blinker2(mut led: Output<'static, P0_14>, interval: Duration) {
    loop {
        led.set_high();
        Timer::after(interval).await;
        led.set_low();
        Timer::after(interval).await;
    }
}

#[embassy::task]
async fn measure(mut temp: Temp<'static>, interval: Duration) {
    loop {
        let v: u16 = temp.read().await.to_num::<u16>();
        info!("Temp: {}°C", v);
        Timer::after(interval).await;
    }
}

#[embassy::main]
async fn main(spawner: Spawner, p: Peripherals) {
    let mut cx = pac::CorePeripherals::take().unwrap();
    cx.DCB.enable_trace();
    cx.DWT.enable_cycle_counter();

    let led1 = Output::new(p.P0_13, Level::Low, OutputDrive::Standard);
    let led2 = Output::new(p.P0_14, Level::Low, OutputDrive::Standard);
    let t = Temp::new(p.TEMP, interrupt::take!(TEMP));

    unwrap!(spawner.spawn(measure(t, Duration::from_millis(2000))));
    unwrap!(spawner.spawn(blinker1(led1, Duration::from_millis(250))));
    unwrap!(spawner.spawn(blinker2(led2, Duration::from_millis(450))));
}
