use crate::hardware_interface::time_since_boot;

#[derive(PartialEq)]
pub enum DelayResult {
    Pending,
    Finished
}
pub struct Delay {
    time_delay_finished: u32
}

impl Delay {
    pub fn new(delay_time_ms: u32) -> Self {
        Delay { 
            time_delay_finished: time_since_boot() + delay_time_ms
        }
    }
}
use core::{future::Future, pin::Pin, task::{Context, Poll}};
impl Future for Delay {
    type Output = ();
    fn poll(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<()> {
        if time_since_boot() > self.time_delay_finished {
            Poll::Ready(())
        }
        else {
            Poll::Pending
        }
    }
}