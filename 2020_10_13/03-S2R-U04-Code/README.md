# S2R Unit 04: Errare Humanum Est - Sed Etiam Computatrum

During this talk, we changed the code in `main.rs` to incrementally include advanced error handling:

* `01-function.rs`: Introducing a helper function with a `Result` as a return type.
* `02-two-types.rs`: Letting the function return two different error types by leveraging a `Box<dyn ...>` (boxed trait).
* `03-two-types-question.rs`: Simplifying the code by leveraging the `?` operator.
* `04-custom-error.rs`: Introduce a custom error type to enable handling the different error cases differently (without a downcast as `Box<dyn ...>` would need).
* `05-thiserror.rs`: Replacing the custom error type that leverages `std` capabilities with the third-party crate `thiserror`.
* `06-anyhow.rs`: Using `color-eyre` (a fork of `anyhow`) to have nice error reporting to show to the user.
