// pub enum Rule {
//     ProhibitedWord(String),
//     MustHaveUmlauts,
//     AcsiiCompatible,
// }
// static UMLAUTS: [char; 4] = ['ä', 'ö', 'ü', 'ß'];
// impl Rule {
//     pub fn compile(&self) -> CompiledExpr {
//         match self {
//             Self::ProhibitedWord(prohibited) => {
//                 CompiledExpr::new(move |ctx| ctx.split_whitespace().all(|w| w != prohibited))
//             }
//             Self::MustHaveUmlauts => {
//                 CompiledExpr::new(move |ctx| ctx.chars().any(|c| UMLAUTS.contains(&c)))
//             }
//             Self::AcsiiCompatible => CompiledExpr::new(move |ctx| ctx.is_ascii()),
//         }
//     }

//     pub fn check(&self, input: &str) -> bool {
//         match self {
//             Self::ProhibitedWord(w) => Self::check_prohibited_word(w, input),
//             Self::MustHaveUmlauts => Self::has_umlauts(input),
//             Self::AcsiiCompatible => Self::is_ascii_compatible(input),
//         }
//     }
//     fn check_prohibited_word(prohibited: &str, input: &str) -> bool {
//         input.split_whitespace().all(|w| w != prohibited)
//     }

//     fn has_umlauts(input: &str) -> bool {
//         input.chars().any(|c| UMLAUTS.contains(&c))
//     }

//     fn is_ascii_compatible(input: &str) -> bool {
//         input.is_ascii()
//     }
// }

// pub struct RuleSet {
//     pub rules: Vec<Rule>,
// }

// impl RuleSet {
//     pub fn check(&self, input: &str) -> bool {
//         self.rules.iter().all(|r| r.check(input))
//     }

//     pub fn compile(&self) -> CompiledExpr {
//         let compiled = self.rules.iter().map(Rule::compile).collect::<Vec<_>>();
//         CompiledExpr::new(move |ctx| compiled.iter().all(|item| item.execute(ctx)))
//     }
// }
