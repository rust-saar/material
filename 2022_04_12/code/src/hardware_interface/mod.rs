
use cortex_m_rt::{entry};
use defmt::error;
use stm32h7xx_hal::{prelude::*, gpio::{gpiob::{PB0, PB14}, Output, PushPull, GpioExt, gpioe::PE1}, timer::{Timer, TimerExt}, device::{TIM2}, rcc::RccExt, pwr::PwrExt};
use embedded_hal::digital::v2::OutputPin;


/// Access the enums variants to modify the user LEDs on the actual board
pub enum UserLEDS {
    LD1,
    LD2,
    LD3
}

impl UserLEDS {
    /// Turn the led on/off (true/false)
    pub fn set_state(&self, led_state: bool) -> () {
        match self {
            &UserLEDS::LD1 => {
                if led_state {
                    InternalState::get().led_1.set_high().unwrap();
                }
                else {
                    InternalState::get().led_1.set_low().unwrap();
                }
            },
            &UserLEDS::LD2 => {
                if led_state {
                    InternalState::get().led_2.set_high().unwrap();
                }
                else {
                    InternalState::get().led_2.set_low().unwrap();
                }
            },
            &UserLEDS::LD3 => {
                if led_state {
                    InternalState::get().led_3.set_high().unwrap();
                }
                else {
                    InternalState::get().led_3.set_low().unwrap();
                }
            },
        }
    }
}

struct InternalState {
    pub led_1: PB0<Output<PushPull>>,
    pub led_2: PE1<Output<PushPull>>,
    pub led_3: PB14<Output<PushPull>>,
    pub timer: Timer<TIM2>
}
// Holds the required peripherals
static mut INTERNAL_STATE: Option<InternalState> = None;

impl InternalState {
    /// Obtain a reference to the structure, used at runtime
    /// 
    /// May panic if called before intialization
    fn get() -> &'static mut Self {
        unsafe {
            INTERNAL_STATE.as_mut().unwrap()
        }
    }

    /// Initialize the global state of the hardware
    fn init(userled_1: PB0<Output<PushPull>>,userled_2: PE1<Output<PushPull>>,userled_3: PB14<Output<PushPull>>, timer: Timer<TIM2>) -> Result<(), ()> {
        let previous = unsafe { INTERNAL_STATE.replace(InternalState {
            led_1: userled_1,
            led_2: userled_2,
            led_3: userled_3,
            timer: timer
        }) };
        if previous.is_some() {
            Err(())
        }
        else {
            Ok(())
        }
    }
}
/// Returns the time passed since boot in milliseconds
pub fn time_since_boot() -> u32 {
    InternalState::get().timer.counter()
}


// Provided by the user during link time
extern "Rust" {
    fn logic() -> ();
}

#[entry]
fn main() -> ! {
    // Get access to the device specific peripherals from the peripheral access crate
    let dp = stm32h7xx_hal::stm32::Peripherals::take().unwrap();

    // Take ownership over the RCC devices and convert them into the corresponding HAL structs
    let rcc = dp.RCC.constrain();

    let pwr = dp.PWR.constrain();
    let pwrcfg = pwr.freeze();

    // Freeze the configuration of all the clocks in the system and
    // retrieve the Core Clock Distribution and Reset (CCDR) object
    let ccdr = rcc.freeze(pwrcfg, &dp.SYSCFG);

    // Acquire the GPIO peripherals
    let gpiob = dp.GPIOB.split(ccdr.peripheral.GPIOB);
    let gpioe = dp.GPIOE.split(ccdr.peripheral.GPIOE);

    // Configure pins as a push-pull output.
    let ld1 = gpiob.pb0.into_push_pull_output();
    let ld2 = gpioe.pe1.into_push_pull_output();
    let ld3 = gpiob.pb14.into_push_pull_output();

    // Configure the timer to count up on every millisecond
    let timer = dp.TIM2.tick_timer(1000.hz(), ccdr.peripheral.TIM2, &ccdr.clocks);
    
    // Setup peripherals for 'static usage'
    InternalState::init(ld1, ld2, ld3, timer).unwrap();

    // Call user code
    unsafe { logic() }

    // Need to loop here because we cannot return from the entry point
    error!("End of life reached!");
    loop {}
}
