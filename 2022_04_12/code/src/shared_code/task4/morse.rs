

use super::{cycle::cycle};

pub async fn morse_a(morse_unit_time: u32) {
    loop {
        cycle(morse_unit_time, morse_unit_time).await;
        cycle(morse_unit_time, morse_unit_time*3).await;
    }
}

pub async fn morse_sos(morse_unit_time: u32) {
    loop {
        cycle(morse_unit_time, morse_unit_time).await;
        cycle(morse_unit_time, morse_unit_time).await;
        cycle(morse_unit_time, morse_unit_time).await;
        cycle(morse_unit_time, morse_unit_time*3).await;
        cycle(morse_unit_time, morse_unit_time*3).await;
        cycle(morse_unit_time, morse_unit_time*3).await;
        cycle(morse_unit_time, morse_unit_time).await;
        cycle(morse_unit_time, morse_unit_time).await;
        cycle(morse_unit_time, morse_unit_time).await;
    }
}

/*
enum InfiniteAMorseState {
    BriefDit,
    LongDat
}

pub struct InfiniteAMorse {
    internal_state: InfiniteAMorseState,
    current_running_cycle: Cycle,
    morse_unit_time: u32
}

impl InfiniteAMorse {
    pub fn new(morse_unit_time: u32) -> InfiniteAMorse {
        InfiniteAMorse { 
            internal_state: InfiniteAMorseState::BriefDit, 
            current_running_cycle: Cycle::new(morse_unit_time, morse_unit_time),
            morse_unit_time: morse_unit_time
        }
    }
}
use core::{future::Future, pin::Pin, task::{Context, Poll}};
impl Future for InfiniteAMorse {
    type Output = ();
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        let self_pointer = self.get_mut();
        if Future::poll(Pin::new(&mut self_pointer.current_running_cycle), cx) == Poll::Ready(()) {
            match self_pointer.internal_state {
                InfiniteAMorseState::BriefDit => {
                    self_pointer.internal_state = InfiniteAMorseState::LongDat;
                    self_pointer.current_running_cycle = Cycle::new(self_pointer.morse_unit_time, self_pointer.morse_unit_time*3);
                },
                InfiniteAMorseState::LongDat => {
                    self_pointer.internal_state = InfiniteAMorseState::BriefDit;
                    self_pointer.current_running_cycle = Cycle::new(self_pointer.morse_unit_time, self_pointer.morse_unit_time);
                }
            }
        }
        Poll::Pending
    }
} 
*/