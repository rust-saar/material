#![no_main]
#![no_std]

use nrf52840_hal::gpio;
use nrf52840_hal::pac::TIMER0;
use nrf52840_hal::prelude::*;
use nrf52840_hal::timer::Instance;
use panic_halt as _;

#[derive(Clone, Copy)]
pub enum State {
    On,
    Off,
}

impl State {
    fn next(self) -> State {
        match self {
            Self::On => Self::Off,
            Self::Off => Self::On,
        }
    }
}

#[rtic::app(device = nrf52840_hal::pac, peripherals = true)]
mod app {
    use super::*;

    #[shared]
    struct Resources {
        state: State,
        led_1: gpio::Pin<gpio::Output<gpio::PushPull>>,
        led_2: gpio::Pin<gpio::Output<gpio::PushPull>>,
        led_3: gpio::Pin<gpio::Output<gpio::PushPull>>,
        led_4: gpio::Pin<gpio::Output<gpio::PushPull>>,
        timer_0: TIMER0,
    }

    #[local]
    struct LocalResources {}

    #[init]
    fn init(cx: init::Context) -> (Resources, LocalResources, init::Monotonics) {
        // Device specific peripherals
        let device: nrf52840_hal::pac::Peripherals = cx.device;

        device.TIMER0.set_periodic();
        device.TIMER0.enable_interrupt();
        device.TIMER0.timer_start(1_000_000u32);

        let port0 = gpio::p0::Parts::new(device.P0);
        let led_1 = port0
            .p0_13
            .into_push_pull_output(gpio::Level::High)
            .degrade();
        let led_2 = port0
            .p0_14
            .into_push_pull_output(gpio::Level::High)
            .degrade();
        let led_3 = port0
            .p0_15
            .into_push_pull_output(gpio::Level::High)
            .degrade();
        let led_4 = port0
            .p0_16
            .into_push_pull_output(gpio::Level::High)
            .degrade();

        (
            Resources {
                state: State::Off,
                led_1,
                led_2,
                led_3,
                led_4,
                timer_0: device.TIMER0,
            },
            LocalResources {},
            init::Monotonics(),
        )
    }

    #[task(binds = TIMER0, shared = [state, timer_0, led_1, led_2, led_3, led_4])]
    fn timer0(cx: timer0::Context) {
        let mut resources = cx.shared;
        resources.timer_0.lock(|t| t.timer_reset_event());

        let mut new_state = PinState::Low;
        resources.state.lock(|state| {
            *state = state.next();

            new_state = match state {
                State::On => PinState::Low,
                State::Off => PinState::High,
            };
        });
        resources
            .led_1
            .lock(|led| led.set_state(new_state).unwrap());
        resources
            .led_2
            .lock(|led| led.set_state(new_state).unwrap());
        resources
            .led_3
            .lock(|led| led.set_state(new_state).unwrap());
        resources
            .led_4
            .lock(|led| led.set_state(new_state).unwrap());
    }
}
