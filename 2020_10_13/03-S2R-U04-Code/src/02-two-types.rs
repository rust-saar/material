use rand::Rng;
use std::cmp::Ordering;
use std::io;
use std::time::Instant;

fn get_guess() -> Result<u32, Box<dyn std::error::Error>> {
    let mut guess = String::new();
    match io::stdin().read_line(&mut guess) {
        Ok(_) => {}
        Err(e) => return Err(Box::new(e)),
    }
    match guess.trim().parse() {
        Ok(t) => Ok(t),
        Err(e) => return Err(Box::new(e)),
    }
}

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);
    let start = Instant::now();
    let end = loop {
        println!("Please input your guess.");

        let guess = match get_guess() {
            Ok(num) => num,
            Err(e) => {
                // no way to match the explicit error type: dyn loses all
                eprintln!("{:#?}", e);
                continue;
            }
        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break Instant::now();
            }
        }
    };
    println!("Took {:?} to guess the correct number.", end - start);
}
