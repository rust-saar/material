use romani::decimal_to_roman;

macro_rules! acceptance_test {
    ($suite:ident, $($name:ident: $input:expr, $output:expr,)*) => {
        mod $suite {
            use super::*;
            $(
                #[test]
                fn $name() -> () {
                    let out = decimal_to_roman($input);
                    assert_eq!($output, out);
                }
            )*
        }
    };
}

acceptance_test!(single_digit,
    one: 1, "I",
    five: 5, "V",
);

acceptance_test!(double_digits,
    two: 2, "II",
    twenty: 20, "XX",
);

acceptance_test!(subtraction,
    four: 4, "IV",
    nine: 9, "IX",
);
