use rand::Rng;
use std::cmp::Ordering;
use std::error::Error;
use std::fmt::Display;
use std::io;
use std::time::Instant;

#[derive(Debug)]
enum CustomError {
    Io,
    Parsing,
}

impl Display for CustomError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            CustomError::Io => f.write_str("Io"),
            CustomError::Parsing => f.write_str("Parsing"),
        }
    }
}

impl From<std::io::Error> for CustomError {
    fn from(_: std::io::Error) -> Self {
        Self::Io {}
    }
}

impl From<std::num::ParseIntError> for CustomError {
    fn from(_: std::num::ParseIntError) -> Self {
        Self::Parsing {}
    }
}

impl Error for CustomError {}

fn get_guess() -> Result<u32, CustomError> {
    let mut guess = String::new();
    io::stdin().read_line(&mut guess)?;

    Ok(guess.trim().parse()?)
}

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);
    let start = Instant::now();
    let end = loop {
        println!("Please input your guess.");

        let guess = match get_guess() {
            Ok(num) => num,
            Err(e) => match e {
                CustomError::Io => panic!("No!"),
                CustomError::Parsing => continue,
            },
        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break Instant::now();
            }
        }
    };
    println!("Took {:?} to guess the correct number.", end - start);
}
