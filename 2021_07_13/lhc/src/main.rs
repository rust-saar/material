use std::{
    thread::{self, sleep},
    time::Duration,
};

fn main() {
    let subs = std::iter::repeat(Scientist {}).take(100).collect();
    foo(subs);
}

/// Fires up the LHC repeatedly and distributes the raw data
///
/// Your task is to run the LHC as often as possible. However, you need to rent your
/// memory from Amazon, which is costly.  Hence, you can only rent enough memory for 
/// the raw data of a single run at once and you need to free the buffer as soon as 
/// possible.
/// After renting the memory and running the LHC, distribute pointers to the raw data
/// to all your Scientist subscribers without copying the data.
/// Make sure that the buffer is released as soon as all Scientists are done with 
/// their analysis.
/// Before running the LHC again, ensure the memory is freed.  You may not check this
/// more than once an hour.
///
/// You also need to implement the `Scientist::do_science` function.
fn foo(subs: Vec<Scientist>) {
    todo!("Room for some setup code.");
    let mut buffer = Amazon::rent_mem();
    LHC::smash_em(&mut buffer);
    todo!("Data is in the buffer, share it with your subs.");
    sleep(Duration::from_secs(3600));
    todo!("Time to clean up and start anew.");
}

#[derive(Clone)]
struct Scientist {}
impl Scientist {
    /// Makes the scientist conduct their scientific science.
    ///
    /// You need to determine the data structure of the research data.
    /// It is safe to assume that it somehow relates to a vec/array of `u8`s.
    /// If necessary, unwrap the data before reading it.
    fn do_science(&self, data: ???) {
        let _ = thread::spawn(move || {
            let raw = todo!("You might need to transform your data somewhat.");
            let count = raw.iter().filter(|b| **b == 0).count();
            assert!(count > 17);
        });
    }
}

struct Amazon {}

impl Amazon {
    fn rent_mem() -> Vec<u8> {
        vec![0u8; 10_000]
    }
}

struct LHC {}

impl LHC {
    fn smash_em(buffer: &mut [u8]) {
        for b in buffer {
            *b = 'a' as u8
        }
    }
}
