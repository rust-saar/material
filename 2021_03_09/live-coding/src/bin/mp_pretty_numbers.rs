use std::thread;

use crossbeam::channel::{self, Sender};
use rand::{thread_rng, Rng};

fn main() {
    let (tx, rx) = channel::unbounded();
    let tx_primes = tx;
    let tx_squares = tx_primes.clone();
    let tx_powers = tx_primes.clone();
    thread::spawn(move || find_prime(tx_primes));
    thread::spawn(move || find_square(tx_squares));
    thread::spawn(move || find_power_of_two(tx_powers));

    rx.into_iter()
        .for_each(|number| println!("Isn't it pretty?? {:?}", number))
}

fn find_prime(chan: Sender<PrettyNumber>) {
    for _ in 0..10000 {
        let candidate = thread_rng().gen::<u16>();
        if check_prime(candidate) {
            register_number(&chan, PrettyNumber::Prime(candidate));
        }
    }
}

fn find_square(chan: Sender<PrettyNumber>) {
    for _ in 0..10000 {
        let candidate = thread_rng().gen::<u16>();
        if (candidate as f64).sqrt().fract() == 0.0 {
            register_number(&chan, PrettyNumber::Square(candidate));
        }
    }
}

fn find_power_of_two(chan: Sender<PrettyNumber>) {
    for _ in 0..10000 {
        let candidate = thread_rng().gen::<u16>();
        if candidate.is_power_of_two() {
            register_number(&chan, PrettyNumber::PowerOfTwo(candidate));
        }
    }
}

fn register_number(chan: &Sender<PrettyNumber>, pn: PrettyNumber) {
    chan.send(pn).unwrap();
}

#[derive(Debug, Clone, PartialEq, Eq)]
enum PrettyNumber {
    Prime(u16),
    PowerOfTwo(u16),
    Square(u16),
}

fn check_prime(count: u16) -> bool {
    //! Doesn't really work, but that suffices for the intention.
    if count.rem_euclid(2) == 0 {
        return false;
    }
    let stop = ((count as f64).sqrt() + 1.0) as u16;
    for i in 3..stop {
        if i.rem_euclid(2) != 0 {
            if count.rem_euclid(i) == 0 {
                return false;
            }
        }
    }
    return true;
}
