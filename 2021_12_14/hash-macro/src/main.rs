macro_rules! hash {
    ($($key:expr => $val:expr)+) => {
        [$(($key, $val),)+]
            .into_iter()
            .collect::<std::collections::HashMap<_, _>>()
    }
}

fn main() {
    let hash = hash! {
        "a" => 2
        "b" => 4
    };
    println!("{:?}", hash);
}
