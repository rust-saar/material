use crate::closure::*;
use rand::Rng;
use std::time::Duration;

type SymTable = Vec<u32>;
type Value = u128;
#[derive(Clone, Debug)]
enum ExprTree {
    Add(Box<ExprTree>, Box<ExprTree>),
    Sub(Box<ExprTree>, Box<ExprTree>),
    Mul(Box<ExprTree>, Box<ExprTree>),
    Exp(Box<ExprTree>, u32),
    Var(usize),
}

impl ExprTree {
    fn eval(&self, st: &SymTable) -> Value {
        match self {
            Self::Add(lhs, rhs) => lhs.eval(st).wrapping_add(rhs.eval(st)),
            Self::Sub(lhs, rhs) => lhs.eval(st).wrapping_sub(rhs.eval(st)),
            Self::Mul(lhs, rhs) => lhs.eval(st).wrapping_mul(rhs.eval(st)),
            Self::Exp(lhs, e) => lhs.eval(st).wrapping_pow(*e),
            Self::Var(ix) => st[*ix] as u128,
        }
    }

    pub fn compile(&self) -> CompiledExpr {
        match self {
            Self::Add(lhs, rhs) => {
                let lhs = lhs.compile();
                let rhs = rhs.compile();
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_add(rhs.execute(ctx)))
            }
            Self::Sub(lhs, rhs) => {
                let lhs = lhs.compile();
                let rhs = rhs.compile();
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_sub(rhs.execute(ctx)))
            }
            Self::Mul(lhs, rhs) => {
                let lhs = lhs.compile();
                let rhs = rhs.compile();
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_mul(rhs.execute(ctx)))
            }
            Self::Exp(lhs, e) => {
                let lhs = lhs.compile();
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_pow(*e))
            }
            Self::Var(ix) => CompiledExpr::new(move |ctx| ctx[*ix] as u128),
        }
    }

    fn create_random<R: Rng>(rng: &mut R, depth: usize, num_var: usize) -> Self {
        if depth == 0 {
            return ExprTree::Var(rng.gen_range(0..num_var));
        }
        let lhs = Box::new(Self::create_random(rng, depth - 1, num_var));
        let kind = rng.gen_range(0..=3);
        if kind == 3 {
            return ExprTree::Exp(lhs, rng.gen());
        }
        let rhs = Box::new(Self::create_random(rng, depth - 1, num_var));
        match kind {
            0 => Self::Add(lhs, rhs),
            1 => Self::Sub(lhs, rhs),
            2 => Self::Mul(lhs, rhs),
            _ => panic!("rng failed"),
        }
    }
}

type ExId = usize;
type VarId = usize;
#[derive(Clone, Debug, Copy)]
enum ExprLinearized {
    Add(ExId, ExId),
    Sub(ExId, ExId),
    Mul(ExId, ExId),
    Exp(ExId, u32),
    Var(VarId),
}

impl ExprLinearized {
    fn eval(&self, st: &SymTable, data: &Vec<ExprLinearized>) -> Value {
        match *self {
            Self::Add(lhs, rhs) => data[lhs]
                .eval(st, data)
                .wrapping_add(data[rhs].eval(st, data)),
            Self::Sub(lhs, rhs) => data[lhs]
                .eval(st, data)
                .wrapping_sub(data[rhs].eval(st, data)),
            Self::Mul(lhs, rhs) => data[lhs]
                .eval(st, data)
                .wrapping_mul(data[rhs].eval(st, data)),
            Self::Exp(lhs, e) => data[lhs].eval(st, data).wrapping_pow(e),
            Self::Var(ix) => st[ix] as u128,
        }
    }

    pub fn compile<'a, 'b, 'c>(&'a self, data: &'b Vec<ExprLinearized>) -> CompiledExpr<'c> {
        match *self {
            Self::Add(lhs, rhs) => {
                let lhs = data[lhs].compile(data);
                let rhs = data[rhs].compile(data);
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_add(rhs.execute(ctx)))
            }
            Self::Sub(lhs, rhs) => {
                let lhs = data[lhs].compile(data);
                let rhs = data[rhs].compile(data);
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_sub(rhs.execute(ctx)))
            }
            Self::Mul(lhs, rhs) => {
                let lhs = data[lhs].compile(data);
                let rhs = data[rhs].compile(data);
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_mul(rhs.execute(ctx)))
            }
            Self::Exp(lhs, e) => {
                let lhs = data[lhs].compile(data);
                CompiledExpr::new(move |ctx| lhs.execute(ctx).wrapping_pow(e))
            }
            Self::Var(ix) => CompiledExpr::new(move |ctx| ctx[ix] as u128),
        }
    }

    fn create_random<R: Rng>(
        rng: &mut R,
        depth: usize,
        num_var: usize,
        data: &mut Vec<ExprLinearized>,
        ix: usize,
    ) -> usize {
        if depth == 0 {
            data[ix] = ExprLinearized::Var(rng.gen_range(0..num_var));
            return ix;
        }
        let lhs = Self::create_random(rng, depth - 1, num_var, data, ix);
        let kind = rng.gen_range(0..=3);
        if kind == 3 {
            let res = lhs + 1;
            data[res] = ExprLinearized::Exp(lhs, rng.gen());
            return res;
        }
        let rhs = Self::create_random(rng, depth - 1, num_var, data, lhs + 1);
        let res = rhs + 1;
        match kind {
            0 => data[res] = Self::Add(lhs, rhs),
            1 => data[res] = Self::Sub(lhs, rhs),
            2 => data[res] = Self::Mul(lhs, rhs),
            _ => panic!("rng failed"),
        }
        return res;
    }
}

pub(crate) fn run_benchmark() {
    main_tree();
    main_lin();
}

static NUM_VARS: usize = 100;
static DEPTH: usize = 15;
static NUM_ITER: usize = 100_000;

fn main_tree() {
    let mut rng = rand::thread_rng();
    let expr = ExprTree::create_random(&mut rng, DEPTH, NUM_VARS);
    let compiled = expr.compile();

    let mut rt_naive = Duration::from_secs(0);
    let mut rt_naive_max = Duration::from_secs(0);
    let mut rt_naive_min = Duration::from_secs(1000);
    let mut rt_compiled = Duration::from_secs(0);
    let mut rt_compiled_max = Duration::from_secs(0);
    let mut rt_compiled_min = Duration::from_secs(1000);

    for _ in 0..NUM_ITER {
        let st = create_sym_table(&mut rng, NUM_VARS);
        let latest_naive = run_naive_tree(&expr, &st);
        rt_naive += latest_naive;
        if latest_naive > rt_naive_max {
            rt_naive_max = latest_naive
        }
        if latest_naive < rt_naive_min {
            rt_naive_min = latest_naive
        }

        let latest_compiled = run_compiled(&compiled, &st);
        if latest_compiled > rt_compiled_max {
            rt_compiled_max = latest_compiled
        }
        if latest_compiled < rt_compiled_min {
            rt_compiled_min = latest_compiled
        }
        rt_compiled += latest_compiled;
    }

    println!(
        "TREE: \t\tNaive: {:?} ({:?} / {:?}), Compiled: {:?} ({:?} / {:?})",
        rt_naive / NUM_ITER as u32,
        rt_naive_min,
        rt_naive_max,
        rt_compiled / NUM_ITER as u32,
        rt_compiled_min,
        rt_compiled_max
    );
}

fn main_lin() {
    let mut rng = rand::thread_rng();
    let mut data = vec![ExprLinearized::Var(0); 2usize.pow(DEPTH as u32)];
    let expr = ExprLinearized::create_random(&mut rng, DEPTH, NUM_VARS, &mut data, 0);
    let expr = data[expr];
    let compiled = expr.compile(&data);

    let mut rt_naive = Duration::from_secs(0);
    let mut rt_compiled = Duration::from_secs(0);
    let mut rt_naive_max = Duration::from_secs(0);
    let mut rt_naive_min = Duration::from_secs(1000);
    let mut rt_compiled_max = Duration::from_secs(0);
    let mut rt_compiled_min = Duration::from_secs(1000);

    for _ in 0..NUM_ITER {
        let st = create_sym_table(&mut rng, NUM_VARS);
        let latest_naive = run_naive_lin(&expr, &st, &data);
        rt_naive += latest_naive;
        if latest_naive > rt_naive_max {
            rt_naive_max = latest_naive
        }
        if latest_naive < rt_naive_min {
            rt_naive_min = latest_naive
        }

        let latest_compiled = run_compiled(&compiled, &st);
        if latest_compiled > rt_compiled_max {
            rt_compiled_max = latest_compiled
        }
        if latest_compiled < rt_compiled_min {
            rt_compiled_min = latest_compiled
        }
        rt_compiled += latest_compiled;
    }

    println!(
        "LINEARIZED: \tNaive: {:?} ({:?} / {:?}), Compiled: {:?} ({:?} / {:?})",
        rt_naive / NUM_ITER as u32,
        rt_naive_min,
        rt_naive_max,
        rt_compiled / NUM_ITER as u32,
        rt_compiled_min,
        rt_compiled_max
    );
}
fn run_naive_tree(e: &ExprTree, st: &SymTable) -> Duration {
    let naive_start = std::time::Instant::now();
    e.eval(&st);
    let naive_end = std::time::Instant::now();
    naive_end.duration_since(naive_start)
}

fn run_naive_lin(e: &ExprLinearized, st: &SymTable, data: &Vec<ExprLinearized>) -> Duration {
    let naive_start = std::time::Instant::now();
    e.eval(&st, data);
    let naive_end = std::time::Instant::now();
    naive_end.duration_since(naive_start)
}

fn run_compiled(e: &CompiledExpr, st: &SymTable) -> Duration {
    let compiled_start = std::time::Instant::now();
    e.execute(&st);
    let compiled_end = std::time::Instant::now();
    compiled_end.duration_since(compiled_start)
}

fn create_sym_table<R: Rng>(rng: &mut R, num_vars: usize) -> SymTable {
    std::iter::repeat_with(|| rng.gen_range(0..3) as u32)
        .take(num_vars)
        .collect()
}
