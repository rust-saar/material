use crate::hardware_interface::UserLEDS;

use super::delay::{Delay};

pub async fn cycle(off_time_ms: u32, on_time_ms: u32) {
    UserLEDS::LD2.set_state(false);
    Delay::new(off_time_ms).await;
    UserLEDS::LD2.set_state(true);
    Delay::new(on_time_ms).await;
}

/*
enum CycleState {
    OffPhase,
    OnPhase
}

pub struct Cycle {
    internal_state: CycleState,
    current_running_delay: Delay,
    on_time_ms: u32
}

#[derive(PartialEq)]
pub enum CycleResult {
    Pending,
    Finished
}
impl Cycle {
    pub fn new(off_time_ms: u32, on_time_ms: u32) -> Self {
        UserLEDS::LD2.set_state(false);
        Cycle {
            internal_state: CycleState::OffPhase,
            current_running_delay: Delay::new(off_time_ms),
            on_time_ms: on_time_ms
        }
    }
}
use core::{future::Future, pin::Pin, task::{Context, Poll}};
impl Future for Cycle {
    type Output = ();
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<()> {
        let inner_pointer = self.get_mut();
        if Future::poll(Pin::new(&mut inner_pointer.current_running_delay), cx) == Poll::Ready(()) {
            match inner_pointer.internal_state {
                CycleState::OffPhase => {
                    inner_pointer.internal_state = CycleState::OnPhase;
                    inner_pointer.current_running_delay = Delay::new(inner_pointer.on_time_ms);
                    UserLEDS::LD2.set_state(true);
                },
                CycleState::OnPhase => {
                    return Poll::Ready(());
                },
            }
        }
        Poll::Pending
    }
}
 */