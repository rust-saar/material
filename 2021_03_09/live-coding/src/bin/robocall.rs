use std::thread;

use rand::{thread_rng, Rng};

fn main() {
    let phone_book = retrieve_phone_book(5);
    let handles: Vec<_> = phone_book
        .into_iter()
        .map(|number| std::thread::spawn(move || robocall(number)))
        .collect();
    handles
        .into_iter()
        .map(thread::JoinHandle::join)
        .map(Result::unwrap)
        .flatten()
        .for_each(print_contract);
}

fn print_contract(c: Contract) {
    println!(
        "New contract with {} over {} yrs",
        c.victim.name, c.duration
    )
}

fn robocall(customer: Customer) -> Option<Contract> {
    println!("{}, calling...", customer.phone);
    if thread_rng().gen_bool(0.1) {
        println!("{}: Entered into a contract.", customer.phone);
        Some(Contract {
            victim: customer,
            duration: thread_rng().gen_range(1..=7),
        })
    } else {
        println!("{}: Not interested.", customer.phone);
        None
    }
}

#[derive(Debug)]
struct Contract {
    duration: usize,
    victim: Customer,
}

#[derive(Debug, Clone)]
struct Customer {
    phone: PhoneNumber,
    name: String,
}

impl From<&(PhoneNumber, &str)> for Customer {
    fn from(d: &(PhoneNumber, &str)) -> Self {
        Customer {
            phone: d.0,
            name: String::from(d.1),
        }
    }
}

type PhoneNumber = &'static str;

fn retrieve_phone_book(how_many: usize) -> Vec<Customer> {
    NUMBERS[..how_many].iter().map(Customer::from).collect()
}

static NUMBERS: [(PhoneNumber, &str); 20] = [
    ("+49 15175 000000", "Robert B. Alexander"),
    ("+49 15175 000001", "Mary T. Gage"),
    ("+49 15175 000002", "Clark J. Primm"),
    ("+49 15175 000003", "Kenneth T. Alexander"),
    ("+49 15175 000004", "Alvin C. Rosales"),
    ("+49 15175 000005", "Johnny V. Sanders"),
    ("+49 15175 000006", "James L. Godines"),
    ("+49 15175 000007", "Stella P. Mercado"),
    ("+49 15175 000008", "Marquerite R. Lopez"),
    ("+49 15175 000009", "Victor H. Devoe"),
    ("+49 15175 000010", "Stan R. Sherman"),
    ("+49 15175 000011", "Alicia B. Coronado"),
    ("+49 15175 000012", "Kelley G. Angulo"),
    ("+49 15175 000013", "June T. Ly"),
    ("+49 15175 000014", "Louis T. Gibson"),
    ("+49 15175 000015", "April R. Bull"),
    ("+49 15175 000016", "Richard R. Williams"),
    ("+49 15175 000017", "Steven L. Barahona"),
    ("+49 15175 000018", "Brandon D. Bradley"),
    ("+49 15175 000019", "Richard P. Pollard"),
];
