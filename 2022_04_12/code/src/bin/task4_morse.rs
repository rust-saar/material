//! # Task 4
//! 
//! Goal:
//! Make LED 1 blink with a 500ms interval
//! and LED 2 morse 'A', with a unit time of 300ms
//! 
//! Morse code for 'A': . _
#![no_std]
#![no_main]
use core::{task::Context, pin::Pin, future::Future};

use defmt_rtt as _; use nucleo_h743zi::{shared_code::{task3::LEDLogic, task4::morse::morse_a}, hardware_interface::UserLEDS};
// global logger
use panic_probe as _; // log panic and halt


#[no_mangle]
fn logic() -> () {
    let waker = nucleo_h743zi::async_helpers::get_stub_waker();
    let mut context = Context::from_waker(&waker);
    let mut led_1 = LEDLogic::new(500, UserLEDS::LD1);
    let mut led_2 = morse_a(300);
    loop {
        led_1.advance_state_for_a_bit();
        let pinned_reference = unsafe { Pin::new_unchecked(&mut led_2) };
        Future::poll(pinned_reference, &mut context);
    }
}