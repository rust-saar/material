use std::fmt::Display;
use rand::Rng;

#[derive(Debug, Copy, Clone)]
enum Rank {
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten,
    Jack,
    Queen,
    King,
    Ace,
}

#[derive(Debug, Copy, Clone)]
enum Suit {
    Spades,
    Diamonds,
    Clubs,
    Hearts,
}


#[derive(Debug, Clone, Copy)]
struct Card {
    rank: Rank,
    suit: Suit,
}

#[derive(Debug)]
struct FrenchDeck {
    cards: Vec<Card>,
}

trait RandomChoice<T> {
    fn choice(&self) -> T;
}

// TODO: specify trait bounds for Type R
impl<T, R: ?> RandomChoice<T> for R {
    fn choice(&self) -> T {
        let lower_bound = 0;
        // get upper bound via a trait method
        let upper_bound = todo!();
        let rnd_idx = rand::thread_rng().gen_range(0..upper_bound);
        // access element at index via a trait method
        todo!()
    }
}



impl Iterator for FrenchDeck {
    type Item = Card;

    fn next(&mut self) -> Option<Self::Item> {
        todo!()
    }
}


fn main() {
    println!("Hello Meetup!");
    let mut fd = FrenchDeck::new();
    println!("Your magic random Card is: {}", fd.choice());
    //println!("The bottom card of your deck is: {}", fd.last().expect("Your deck should not be empty"));
    //println!("The 7-th card of your deck is: {}", fd.nth(7).expect("Your deck should big enough"));
    /* 
    println!("I only want Kings!");
    fd.filter(|c|matches!(c.rank, Rank::King)).for_each(|c|println!("{}",c));
    */
}

//-------------- Struct impl blocks -----------------
impl Suit {
    pub fn iterator() -> impl Iterator<Item = Suit> {
        use Suit::*;
        [Spades, Diamonds, Clubs, Hearts].iter().copied()
    }

    pub fn to_string(&self) -> String {
        use Suit::*;
        match self {
            Spades => "♠",
            Diamonds => "♦",
            Clubs => "♣",
            Hearts => "♥",
        }.to_string()
    }
}

impl Rank {
    pub fn iterator() -> impl Iterator<Item = Rank> {
        use Rank::*;
        [
            Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace,
        ]
        .iter()
        .copied()
    }
    pub fn to_string(&self) -> String {
        use Rank::*;
        match self {
            Two => "2",
            Three => "3",
            Four => "4",
            Five => "5",
            Six => "6",
            Seven => "7",
            Eight => "8",
            Nine => "9",
            Ten => "10",
            Jack => "J",
            Queen => "Q",
            King => "K",
            Ace => "A",
        }.to_string()
    }
}


impl Card {
    fn new(suit: Suit, rank : Rank) -> Self {
        Card { rank, suit }
    }

    pub fn to_string(&self) -> String {
        self.rank.to_string() + &self.suit.to_string()
    }
}


impl FrenchDeck {
    fn new() -> Self {
        let cards = Suit::iterator().flat_map(|s| Rank::iterator().map(|r| Card::new(s, r)).collect::<Vec<_>>()).collect();
        FrenchDeck { cards, _cur: 0 }
    }
}




//---------------- Pretty Print impl --------------------
impl Display for Card {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

impl Display for Rank {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}

impl Display for Suit {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.to_string())
    }
}
