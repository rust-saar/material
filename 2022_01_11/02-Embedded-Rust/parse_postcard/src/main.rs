use postcard::from_bytes;
use serde::{Deserialize, Serialize};
use uom::si::f32::ThermodynamicTemperature;

#[derive(Serialize, Deserialize, Debug)]
pub struct Sample {
    pub time: u32,
    pub temperature: ThermodynamicTemperature,
}

fn main() {
    let bytes = &[39, 223, 18, 19, 51, 115, 148, 67];
    let sample: Sample = from_bytes(bytes).unwrap();
    dbg!(sample);
}
