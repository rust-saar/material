# Sugar-Sweet Concurrency: No need to wait for your results!

This folder holds the code from the Rust-Saar Meetup Talk 20u16, held by
Finn Hermeling.

This is a fork from https://github.com/astraw/nucleo-h743zi. There you can find
information how to get the setup running for yourself.