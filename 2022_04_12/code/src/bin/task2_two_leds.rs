//! # Task 2
//! 
//! Goal:
//! Make LED 1 blink with a 500ms interval and LED 2 blink with a 250ms interval
#![no_std]
#![no_main]
use defmt_rtt as _; use nucleo_h743zi::hardware_interface::{UserLEDS, time_since_boot};
// global logger
use panic_probe as _; // log panic and halt

fn delay(delay_time_ms: u32) {
    let start_time = time_since_boot();
    while time_since_boot() < start_time + delay_time_ms {}
}

#[no_mangle]
fn logic() -> () {
    loop {
        UserLEDS::LD1.set_state(true);
        UserLEDS::LD2.set_state(true);
        delay(250);
        UserLEDS::LD2.set_state(false);
        delay(250);

        UserLEDS::LD1.set_state(false);
        UserLEDS::LD2.set_state(true);
        delay(250);
        UserLEDS::LD2.set_state(false);
        delay(250);
        
    }
}