use std::{
    sync::{Arc, Mutex},
    thread,
    time::Duration,
};

use rand::{thread_rng, Rng};

fn main() {
    let max = Arc::new(Mutex::new(0u16));
    let max_1 = max.clone();
    let max_2 = max.clone(); // One clone is unnecessary of course
    let handle_1 = thread::spawn(|| {
        find_max(1, max_1);
    });
    let handle_2 = thread::spawn(|| {
        find_max(2, max_2);
    });
    handle_1.join().unwrap();
    handle_2.join().unwrap();
    println!("Max: {}", *max.lock().unwrap())
}

fn find_max(id: usize, mutex: Arc<Mutex<u16>>) {
    println!("T{} started.", id);
    for _ in 0..10000 {
        let next = thread_rng().gen::<u16>();
        {
            let mut max = mutex.lock().unwrap();
            if next > *max {
                *max = next;
                println!("T{} found next greatest: {}", id, max);
            }
            thread::sleep(Duration::from_micros(1));
        }
        // {
        //     if next > *mutex.lock().unwrap() {
        //         // thread::sleep(Duration::from_micros(1));
        //         *mutex.lock().unwrap() = next;
        //         println!("T{} found next greatest: {}", id, *mutex.lock().unwrap());
        //     }
        // }
    }
    println!("T{} terminated.", id);
}
