fn main() {
    let args = &std::env::args().into_iter().collect::<Vec<String>>()[1..];
    let (path, length) = match args.len() {
        2 => {
            let path = args.get(0).unwrap();
            let length = args.get(1).unwrap();
            let length = length
                .parse()
                .unwrap_or_else(|_| panic!("Couldn not parse {} to number.", length));
            (path, length)
        }
        _ => panic!("Must be called with 2 parameters: PATH LENGTH."),
    };

    let words: Vec<String> = std::fs::read_to_string(path)
        .unwrap_or_else(|_| panic!("Could not read from file {}.", path))
        .split_whitespace()
        .map(|w| w.to_string())
        .flat_map(|s| {
            s.as_bytes()
                .chunks(length)
                .map(|w| String::from_utf8(w.into()).unwrap())
                .collect::<Vec<String>>()
        })
        .collect();

    let mut lines = vec![];
    let mut line: Vec<String> = vec![];
    for word in words {
        if line.iter().map(|w| w.len()).sum::<usize>() + line.len() * 1 + word.len() <= length {
            line.push(word);
        } else {
            lines.push(line);
            line = vec![word];
        }
    }
    lines.push(line);

    let formatted = lines
        .into_iter()
        .map(|l| format!("{:^length$}", l.join(" ").to_string(), length = length))
        .collect::<Vec<String>>()
        .join("\n");

    println!("{}", formatted)
}
