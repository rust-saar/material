use crate::{SymTable, Value};

pub(crate) type ExecutionContext<'s> = SymTable;
pub(crate) struct CompiledExpr<'s>(Box<dyn 's + Fn(&ExecutionContext<'s>) -> Value>);

impl<'s> CompiledExpr<'s> {
    pub(crate) fn new(closure: impl 's + Fn(&ExecutionContext<'s>) -> Value) -> Self {
        CompiledExpr(Box::new(closure))
    }

    pub(crate) fn execute(&self, ctx: &ExecutionContext<'s>) -> Value {
        self.0(ctx)
    }
}
