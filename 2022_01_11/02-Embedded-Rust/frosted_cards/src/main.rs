#![no_main]
#![no_std]

use defmt_rtt as _; // global logger
use nrf52840_hal::pac::{self, TIMER0};
use nrf52840_hal::prelude::*;
use nrf52840_hal::timer::Instance;
use nrf52840_hal::{gpio, Temp};
use panic_probe as _;

defmt::timestamp!("{=u32:us}", pac::DWT::get_cycle_count().into());

use postcard::to_vec;
use serde::{Deserialize, Serialize};
use uom::si::f32::ThermodynamicTemperature;
use uom::si::thermodynamic_temperature;

mod mono;

#[derive(Serialize, Deserialize)]
pub struct Sample {
    pub time: u32,
    pub temperature: ThermodynamicTemperature,
}

#[rtic::app(device = nrf52840_hal::pac, peripherals = true)]
mod app {
    use rtic::Monotonic;

    use super::*;

    #[shared]
    struct Resources {
        led_1: gpio::Pin<gpio::Output<gpio::PushPull>>,
        led_2: gpio::Pin<gpio::Output<gpio::PushPull>>,
        led_3: gpio::Pin<gpio::Output<gpio::PushPull>>,
        led_4: gpio::Pin<gpio::Output<gpio::PushPull>>,
        temp: Temp,
        timer_0: mono::MonoTimer<TIMER0>,
    }

    #[local]
    struct LocalResources {}

    #[init]
    fn init(cx: init::Context) -> (Resources, LocalResources, init::Monotonics) {
        // Device specific peripherals
        let device: nrf52840_hal::pac::Peripherals = cx.device;

        let temp = Temp::new(device.TEMP);

        device.TIMER0.set_periodic();
        device.TIMER0.enable_interrupt();
        device.TIMER0.timer_start(100_000_000u32);

        let port0 = gpio::p0::Parts::new(device.P0);
        let led_1 = port0
            .p0_13
            .into_push_pull_output(gpio::Level::High)
            .degrade();
        let led_2 = port0
            .p0_14
            .into_push_pull_output(gpio::Level::High)
            .degrade();
        let led_3 = port0
            .p0_15
            .into_push_pull_output(gpio::Level::High)
            .degrade();
        let led_4 = port0
            .p0_16
            .into_push_pull_output(gpio::Level::High)
            .degrade();

        (
            Resources {
                led_1,
                led_2,
                led_3,
                led_4,
                temp,
                timer_0: mono::MonoTimer::new(device.TIMER0),
            },
            LocalResources {},
            init::Monotonics(),
        )
    }

    #[task(binds = TIMER0, shared = [timer_0, led_1, led_2, led_3, led_4, temp])]
    fn timer0(mut cx: timer0::Context) {
        let measurement: f32 = cx.shared.temp.lock(|temp| temp.measure()).to_num();
        let sample = Sample {
            temperature: ThermodynamicTemperature::new::<thermodynamic_temperature::degree_celsius>(
                measurement,
            ),
            time: cx.shared.timer_0.lock(|timer| timer.now()).ticks(),
        };

        defmt::info!(
            "Debug: {}, TEMP: {}K, {}°C",
            sample.time,
            sample
                .temperature
                .get::<thermodynamic_temperature::kelvin>(),
            sample
                .temperature
                .get::<thermodynamic_temperature::degree_celsius>()
        );

        // Serialize
        let buffer: heapless::Vec<u8, 40> = to_vec(&sample).unwrap();
        defmt::info!("Postcard: {=[u8]}", buffer);

        // LEDs
        let state = if measurement >= 25.0 {
            PinState::Low
        } else {
            PinState::High
        };
        let mut resources = cx.shared;
        resources.led_1.lock(|led| led.set_state(state).unwrap());
        resources.led_2.lock(|led| led.set_state(state).unwrap());
        resources.led_3.lock(|led| led.set_state(state).unwrap());
        resources.led_4.lock(|led| led.set_state(state).unwrap());
    }
}
