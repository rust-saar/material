use color_eyre::Result;
use rand::Rng;
use std::cmp::Ordering;
use std::io;
use std::time::Instant;
use thiserror::Error;

#[non_exhaustive]
#[derive(Error, Debug)]
enum CustomError {
    #[error("IO")]
    Io(#[from] std::io::Error),
    #[error("Parsing : {0:#?}")]
    Parsing(#[from] std::num::ParseIntError),
}

fn get_guess() -> Result<u32, CustomError> {
    let mut guess = String::new();
    io::stdin().read_line(&mut guess)?;

    Ok(guess.trim().parse()?)
}

fn main() -> Result<()> {
    color_eyre::install()?;
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1, 101);
    let start = Instant::now();
    let end = loop {
        println!("Please input your guess.");

        let guess = match get_guess() {
            Ok(num) => num,
            Err(e) => Err(e)?,
        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break Instant::now();
            }
        }
    };
    println!("Took {:?} to guess the correct number.", end - start);
    Ok(())
}
