//! # Task 3
//! 
//! Goal:
//! Make LED 1 blink with a 500ms interval and LED 2 blink with a 213ms interval
//! 
#![no_std]
#![no_main]
use defmt_rtt as _; use nucleo_h743zi::{hardware_interface::UserLEDS, shared_code::task3::LEDLogic};
// global logger
use panic_probe as _; // log panic and halt

#[no_mangle]
fn logic() -> () {
    let mut led_1 = LEDLogic::new(500, UserLEDS::LD1);
    let mut led_2 = LEDLogic::new(100, UserLEDS::LD2);
    loop {
        led_1.advance_state_for_a_bit();
        led_2.advance_state_for_a_bit();
    }
}