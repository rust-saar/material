use core::task::{RawWakerVTable, RawWaker, Waker};


const STUBWAKER: RawWakerVTable = RawWakerVTable::new(clone, wake, wake_by_ref, drop);

unsafe fn clone(_: *const ()) -> RawWaker {
    RawWaker::new(core::ptr::null(), &STUBWAKER)
}

unsafe fn wake(_: *const ()) {
    
}

unsafe fn wake_by_ref(_: *const ()) {
    
}

unsafe fn drop(_: *const ()) {
    
}

pub fn get_stub_waker() -> Waker {
    unsafe { Waker::from_raw(RawWaker::new(core::ptr::null(), &STUBWAKER)) }
}