fn roman_digit_to_decimal(digit: &str) -> i64 {
    match digit {
        "I" => 1,
        "IV" => 4,
        "V" => 5,
        "IX" => 9,
        "X" => 10,
        _ => unimplemented!("{}", digit),
    }
}

pub fn roman_to_decimal(roman: &str) -> i64 {
    let digits = roman.chars().into_iter().collect::<Vec<_>>();

    let subtraction: i64 = digits
        .iter()
        .zip(digits.iter().skip(1))
        .map(|(l, r)| {
            let (l, r) = (
                roman_digit_to_decimal(&l.to_string()),
                roman_digit_to_decimal(&r.to_string()),
            );
            if l < r {
                -2 * l
            } else {
                0
            }
        })
        .sum();

    digits
        .into_iter()
        .map(|digit| roman_digit_to_decimal(&digit.to_string()))
        .sum::<i64>()
        + subtraction
}

pub fn decimal_to_roman(mut decimal: i64) -> String {
    let digits = vec!["X", "IX", "V", "IV", "I"];

    let mut answer = String::new();

    for digit in digits {
        let single = roman_digit_to_decimal(digit);
        loop {
            if single <= decimal {
                answer.push_str(digit);
                decimal -= single;
            } else {
                break;
            }
        }
    }
    answer
}
