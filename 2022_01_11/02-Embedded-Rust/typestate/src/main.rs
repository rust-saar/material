mod gpio {
    use std::marker::PhantomData;

    // States as unit-like structs
    pub struct Uninitialized;
    pub struct Input;
    pub struct Output;

    pub struct Pin<T> {
        state: PhantomData<T>,
    }

    impl Pin<Uninitialized> {
        pub fn new(register_address: u32) -> Self {
            // actually use register_address
            Self {
                state: Default::default(),
            }
        }

        pub fn to_input(self) -> Pin<Input> {
            // write to register
            Pin {
                state: Default::default(),
            }
        }

        pub fn to_output(self) -> Pin<Output> {
            // write to register
            Pin {
                state: Default::default(),
            }
        }
    }

    impl Pin<Input> {
        pub fn read(&mut self) -> u8 {
            // access registers
            42
        }
    }

    impl Pin<Output> {
        pub fn write(&mut self, value: u8) -> () {
            // access registers
            println!("Output: {}", value);
        }
    }

    // constants from data sheet
    pub const PIN1: u32 = 4711;
    pub const PIN2: u32 = 815;
}

use gpio::*;

fn main() {
    let pin = Pin::new(PIN1);

    let mut input_pin = pin.to_input();
    println!("Input: {}", input_pin.read());

    let pin = Pin::new(PIN2);
    let mut output_pin = pin.to_output();
    output_pin.write(47);
}
