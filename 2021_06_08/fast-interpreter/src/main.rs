mod closure;
use closure::*;
use rand::Rng;

type SymTable = Vec<u32>;
type Value = u128;
#[derive(Clone, Debug)]
enum ExprTree {
    Add(Box<ExprTree>, Box<ExprTree>),
    // Sub(Box<ExprTree>, Box<ExprTree>),
    // Mul(Box<ExprTree>, Box<ExprTree>),
    Exp(Box<ExprTree>, u32),
    Var(usize),
}

impl ExprTree {
    fn eval(&self, st: &SymTable) -> Value {
        match self {
            Self::Add(lhs, rhs) => lhs.eval(st) + rhs.eval(st),
            Self::Exp(lhs, e) => lhs.eval(st).pow(*e),
            Self::Var(vx) => st[*vx] as u128,
        }
    }

    pub fn compile(&self) -> CompiledExpr {
        match self {
            Self::Add(lhs, rhs) => {
                let lhs = lhs.compile();
                let rhs = rhs.compile();
                CompiledExpr::new(move |st| lhs.execute(st) + rhs.execute(st))
            }
            Self::Exp(lhs, e) => {
                let lhs = lhs.compile();
                CompiledExpr::new(move |st| lhs.execute(st).pow(*e))
            }
            Self::Var(vx) => CompiledExpr::new(move |st| st[*vx] as u128),
        }
    }

    fn create_random<R: Rng>(rng: &mut R, depth: usize, num_var: usize) -> Self {
        unimplemented!("does not matter here");
    }
}

type ExId = usize;
type VarId = usize;
#[derive(Clone, Debug, Copy)]
enum ExprLinearized {
    Add(ExId, ExId),
    // Sub(ExId, ExId),
    // Mul(ExId, ExId),
    Exp(ExId, u32),
    Var(VarId),
}

impl ExprLinearized {
    fn eval(&self, st: &SymTable, data: &Vec<ExprLinearized>) -> Value {
        todo!()
    }

    pub fn compile<'a, 'b, 'c>(&'a self, data: &'b Vec<ExprLinearized>) -> CompiledExpr<'c> {
        todo!()
    }

    fn create_random<R: Rng>(
        rng: &mut R,
        depth: usize,
        num_var: usize,
        data: &mut Vec<ExprLinearized>,
        ix: usize,
    ) -> usize {
        unimplemented!("does not matter here");
    }
}

mod main_full;
fn main() {
    main_full::run_benchmark();
}
