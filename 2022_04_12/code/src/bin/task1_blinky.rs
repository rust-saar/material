//! # Task 1
//! 
//! Goal:
//! Make LED 1 blink with cycle 500ms off, 500ms on
//! 
#![no_std]
#![no_main]

use defmt_rtt as _; // for potential log messages
use panic_probe as _; // for logging and halting on panics
use nucleo_h743zi::hardware_interface::{time_since_boot, UserLEDS}; // Interface with hardware

fn delay(delay_time_ms: u32) {
    let start_time = time_since_boot();
    while time_since_boot() < start_time + delay_time_ms {}
}

#[no_mangle]
fn logic() -> () {
    loop {
        UserLEDS::LD1.set_state(true);
        delay(500);

        UserLEDS::LD1.set_state(false);
        delay(500);
        
    }
}