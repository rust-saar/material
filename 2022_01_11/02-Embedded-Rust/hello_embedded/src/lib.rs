#![no_std]
use core::sync::atomic::{AtomicUsize, Ordering};
use defmt_rtt as _; // global logger
use nrf52840_hal as _; // use some_hal as _; // memory layout
use panic_probe as _;

#[defmt::panic_handler]
fn panic() -> ! {
    cortex_m::asm::udf()
}
static COUNT: AtomicUsize = AtomicUsize::new(0);
defmt::timestamp!("{=usize}", {
    // NOTE(no-CAS) `timestamps` runs with interrupts disabled
    let n = COUNT.load(Ordering::Relaxed);
    COUNT.store(n + 1, Ordering::Relaxed);
    n
});
pub fn exit() -> ! {
    loop {
        cortex_m::asm::bkpt();
    }
}
