use crate::hardware_interface::{UserLEDS, time_since_boot};
enum LEDLogicState {
    On,
    Off
}

pub struct LEDLogic {
    internal_state: LEDLogicState,
    next_state_change: u32,
    controlled_led: UserLEDS,
    interval_time: u32
}

impl LEDLogic {
    pub fn new(interval_time: u32, controlled_led: UserLEDS) -> Self {
        LEDLogic { 
            internal_state: LEDLogicState::Off, 
            next_state_change: time_since_boot() + interval_time,
            interval_time: interval_time,
            controlled_led: controlled_led
        }
    }

    pub fn advance_state_for_a_bit(&mut self) -> () {
        let current_time = time_since_boot();
        if current_time > self.next_state_change {
            match self.internal_state {
                LEDLogicState::On => {
                    self.controlled_led.set_state(false);
                    self.internal_state = LEDLogicState::Off;
                    self.next_state_change = current_time + self.interval_time;
                },
                LEDLogicState::Off => {
                    self.controlled_led.set_state(true);
                    self.internal_state = LEDLogicState::On;
                    self.next_state_change = current_time + self.interval_time;
                },
            }
        }
    }
}