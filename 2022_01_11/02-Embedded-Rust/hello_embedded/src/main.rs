#![no_main]
#![no_std]
use hello_embedded as _; // global logger + panicking-behavior + memory layout

#[cortex_m_rt::entry]
fn main() -> ! {
    defmt::info!("Hello, world!");
    defmt::info!("Hello, world2!");

    hello_embedded::exit()
}
