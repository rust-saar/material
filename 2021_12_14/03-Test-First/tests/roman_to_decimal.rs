use romani::roman_to_decimal;

macro_rules! acceptance_test {
    ($suite:ident, $($name:ident: $input:expr, $output:expr,)*) => {
        mod $suite {
            use super::*;
            $(
                #[test]
                fn $name() -> () {
                    let out = roman_to_decimal($input);
                    assert_eq!($output, out);
                }
            )*
        }
    };
}

acceptance_test!(single_digit,
    one: "I", 1,
    five: "V", 5,
    ten: "X", 10,
);

acceptance_test!(double_digits,
    two: "II", 2,
    twenty: "XX", 20,
);

acceptance_test!(subtracing_digits,
    four: "IV", 4,
    nine: "IX", 9,
);
