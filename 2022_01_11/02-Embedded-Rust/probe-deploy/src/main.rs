use probe_rs::flashing::{download_file, Format};
use probe_rs::Probe;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let probes = Probe::list_all();

    let probe = probes[0].open()?;

    let mut session = probe.attach("nrf52840")?;

    download_file(
        &mut session,
        std::path::Path::new("./blinky_rtic"),
        Format::Elf,
    )
    .unwrap();
    Ok(())
}
